#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @input = <STDIN>;

my $sum = 0;

foreach my $line (@input){
	my @line_values = $line =~ /(\d+)/g;

	my ($high_value, $low_value) = find_highlow( @line_values );

	say "> $high_value / $low_value = " . ($high_value / $low_value);

	$sum += $high_value / $low_value;
}

say "Total is $sum";



sub find_highlow {
	my @line_values = sort { $b <=> $a } @_;


	for ( my $i = 0; $i < $#line_values - 1; $i++ ){
		for ( my $j = $i + 1; $j <= $#line_values; $j++ ){
			if( $line_values[$i] / $line_values[$j] == int( $line_values[$i] / $line_values[$j] ) ){
				return ( $line_values[$i], $line_values[$j] );
			}

		}
	}
	die("Can't find high/low in " . join(', ', @line_values));
}