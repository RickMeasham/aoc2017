#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

# Circular 
my $input = $ARGV[0] . substr( $ARGV[0], 0, 1);

# Positive lookahead using the current match
my @matches = $input =~ m/(\d)(?=\1)/g;

# Add 'em up
my $sum = 0;
$sum += $_ foreach @matches;

# Print 'em out
say join(' + ', @matches) . ' = ' . $sum;

