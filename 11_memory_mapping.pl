#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @banks = split /\s+/ shift;

my %seen;
my $loops = 0;


while(1){
	say join(' ', @banks);

	my $this_key = join(' ', @banks);
	if( $seen{ $this_key } ){
		say "Found a duplicate in $loops moves. Loop is " . ($loops - $seen{ $this_key }) . " cycles";
		exit;
	}

	$seen{ $this_key } = $loops;

	redistribute( highest_block() );

	$loops++;
}


sub highest_block {

	my $highest_block_id = 0;
	my $highest_block_value = 0;

	for( my $i = 0; $i < @banks; $i++ ){
		if( $highest_block_value < $banks[$i] ){
			$highest_block_value = $banks[$i];
			$highest_block_id = $i;
		}
	}
	say "Highest block is $highest_block_id ($highest_block_value)";
	return $highest_block_id;
}


sub redistribute {
	my $block = shift;

	my $num_to_distribute = $banks[ $block ];
	$banks[ $block ] = 0;

	while( $num_to_distribute ){
		$block = ( $block + 1 ) % scalar( @banks );

		$banks[ $block ]++;

		$num_to_distribute--;
	}
}

