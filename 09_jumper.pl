#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @tape = map { chomp; $_ } <STDIN>;

my $jumps = 0;
my $pointer = 0;
while( $pointer >= 0 && $#tape >= $pointer ){

	# Don't enable this on long lists. It's not readable, and it takes far more processing than the actual 1D maze.
	# print_tape( \@tape, $pointer );

	my $jump = $tape[ $pointer ];

	$tape[ $pointer ]++;

	$pointer += $jump;

	$jumps++;
}

say "Escaped in $jumps jumps.";


sub print_tape {
	my ($tape, $pointer) = @_;

	for( 0 .. $pointer - 1){
		print ' ' . $tape->[ $_ ] . '  ';
	}
	print '(' . $tape->[ $pointer ] . ') ';
	for( $pointer + 1 .. $#$tape ){
		print ' ' . $tape->[ $_ ] . '  ';
	}
	print "\n";
}

