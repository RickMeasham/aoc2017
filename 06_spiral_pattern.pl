#!/usr/bin/env perl

# This solution uses the simplest maze solving concept of keeping your hand on the left wall. We place a tile 
# and, if there' no wall (in this case an existing value) to the left, we'll turn left. Otherwise we keep moving 
# forward.
#
# So we start at the (1) and move 'forward' with our current vector of moving east. We place the next '1' and as there's
# no value to the left (north) we turn left. We place the '2' and as there's no value to the left (now west) we turn left.
# Repeat ad-infinitum until you find a value higher than you're searching for.
#
# ┌─────────────────────────┐
# │ 147  142  133  122   59 │
# │     ┌──────────────┐    │   
# │ 304 │  5    4    2 │ 57 │
# │     │    ┌─────    │    │      
# │ 330 │ 10 │  1    1 │ 54 │
# │     │    └─────────┘    │
# │ 351 │ 11   23   25   26 │ 
# │     └───────────────────┘    ┊
# │ 362  747  806--->   ...      ┆
# └──────────────────────────────┘


use 5.20.0;
use strict;
use warnings;

use Data::Dumper;
use Text::Table::Tiny qw/ generate_table /;

my $input = shift;

# Seed the center tile
my %grid = (
	0 => { 0 => 1 }
);

# Values that change as we spiral
my $x = 0; my $y = 0; # Current tile
my $vector = [1,0];   # Current direction

# Loop breaks when we find the value we want .. or we run out of memory (which wont happen as numbers get big fast)
while(1){

	# Move forward
	( $x, $y ) = add_vector( $x, $y, $vector );

	# Lay Egg
	$grid{ $x }{ $y } = gridsum( \%grid, $x, $y );

	if( $grid{ $x }{ $y } > $input ){
		print_grid( \%grid, $x, $y );
		exit;
	}

	# If nothing to the left, turn left
	if( ! left_value( \%grid, $vector, $x, $y ) ){
		$vector = turn_left( $vector );
	}

}

# Return true if the tile to the left (facing in the vector direction) has a value
sub left_value {
	my ($grid, $vector, $x, $y ) = @_;

	my $temp_vector = turn_left( $vector ); 
	( $x, $y ) = add_vector( $x, $y, $temp_vector );

	return exists $grid->{$x}{$y};
}

# Add the vector to the given location. Return the new location
sub add_vector {
	my ( $x, $y, $vector ) = @_;
	$x += $vector->[0];
	$y += $vector->[1];
	return $x, $y;
}

# Given the current vector, turn left and return the new vector
sub turn_left {
	my $vector = shift;
	return {
		'1,0'  => [ 0, 1 ],
		'0,1'  => [-1, 0 ],
		'-1,0' => [ 0,-1 ],
		'0,-1' => [ 1, 0 ]
	}->{ join(',', @$vector ) };
}

# Return the sum of the cells surrounding the given cell
sub gridsum{
	my ( $grid, $x, $y ) = @_;

	my $val = 0;
	$val += $grid->{ $x - 1 }{ $y - 1 } // 0;
	$val += $grid->{ $x     }{ $y - 1 } // 0;
	$val += $grid->{ $x + 1 }{ $y - 1 } // 0;

	$val += $grid->{ $x - 1 }{ $y     } // 0;
	# current square is $grid->{ $x }{ $y }
	$val += $grid->{ $x + 1 }{ $y     } // 0;

	$val += $grid->{ $x - 1 }{ $y + 1 } // 0;
	$val += $grid->{ $x     }{ $y + 1 } // 0;
	$val += $grid->{ $x + 1 }{ $y + 1 } // 0;

	return $val;
}

sub print_grid {
	my ($grid, $x, $y) = @_;

	my $table_size = abs( abs( $x ) > abs( $y ) ? $x : $y );

	my @table;
	for my $x ( -$table_size .. $table_size ){
		for my $y ( -$table_size .. $table_size ){
			$table[ $y + $table_size ][ $x + $table_size ] = $grid->{$x}{$y} // ' ';
		}
	}
	# It's upside down Miss Jane!
	@table = reverse @table;

	say generate_table(rows => \@table, header_row => 0);
}