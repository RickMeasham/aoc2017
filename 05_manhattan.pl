#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my $input = shift;
$input--;

my $low_sqrt = int( sqrt $input );
$low_sqrt-- unless $low_sqrt % 2;
my $low_ring = $low_sqrt ** 2 + 1;

my $high_sqrt = int( sqrt $input ) + 1;
$high_sqrt++ unless $high_sqrt % 2;
my $high_ring = $high_sqrt ** 2;
say $input+1 . " is on the ring from $low_ring to $high_ring";

my $ring = int( (sqrt $low_ring) / 2 ) + 1;
say "which is ring $ring";

my $edge = $high_sqrt - 1;
say "Ring $ring has an edge of $edge";

my $side = int( ( ($input+1) - $low_ring) / $edge );

my $low_side = $side * $edge + $low_ring - 1;
my $high_side = $low_side + $edge;
say "and it is on side $side which goes from $low_side to $high_side";

my $side_pos = $input % ($edge-1) + 1;
my $center_tile = ($low_side + $high_side) / 2;
say "whose center tile is $center_tile";

my $center_delta = $input+1 - $center_tile;
$center_delta*=-1 if $center_delta < 0;
say "which is a travel of $center_delta";

say "The manhattan distance is therefore $center_delta + $ring = " . ( $center_delta + $ring );

