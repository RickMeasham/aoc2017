#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @input = <STDIN>;

my $sum = 0;

foreach my $line (@input){
	my @line_values = $line =~ /(\d+)/g;

	@line_values = sort {$a <=> $b} @line_values;
	say join(', ', @line_values);

	my $high_value = $line_values[$#line_values];
	my $low_value = $line_values[0];
	say "> $high_value - $low_value = " . ($high_value - $low_value);

	$sum += $high_value - $low_value;
}

say "Total is $sum";
