#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @input = <STDIN>;

my $valid = 0;
foreach my $line ( @input ){
	my @words = split /\s+/, $line;

	my %words;
	@words{ @words } = ();

	if( scalar( @words ) == scalar( keys %words ) ){
		$valid++ if no_anagrams( @words );
	}
}

say "There are $valid passphrases.";


sub no_anagrams {
	my @words = map { join('', sort( split //, $_ ) ) } @_;

	my %words;
	@words{ @words } = ();

	return scalar( @words ) == scalar( keys %words );
}
