#!/usr/bin/env perl

use 5.10.0;
use strict;
use warnings;

use Data::Dumper;

my @input = <>;

my %register;
my $high_value = 0;

foreach my $line ( @input ){
    next unless $line =~ /\d/;

    my( $register, $direction, $value, $other_register, $operation, $comparison_value)
        = $line =~ /(\w+) (inc|dec) (-?\d+) if (\w+) ([<>=!]+) (-?\d+)/;

    # Deal with the direction and be done with it
    $value *= -1 if $direction eq 'dec';

    # Initialise a new register
    $register{ $register } //= 0;

    printf "I will add $value to $register, but only if $other_register [$operation] $comparison_value\n";

    if( check( $other_register, $operation, $comparison_value ) ){
        $register{ $register } += $value;
    }

    $high_value = $register{ $register } if $register{ $register } > $high_value;
}

my @keys = sort { $register{$b} <=> $register{$a} } keys %register;
printf("%s: %d\n", $_, $register{$_}) foreach @keys;

printf "The highest value during the process was %d\n", $high_value;



sub check {
    my ( $other_register, $operation, $comparison_value ) = @_;

    return ( $operation eq '==' && $register{$other_register} == $comparison_value)
        || ( $operation eq '!=' && $register{$other_register} != $comparison_value)
        || ( $operation eq '<' && $register{$other_register} < $comparison_value)
        || ( $operation eq '>' && $register{$other_register} > $comparison_value)
        || ( $operation eq '>=' && $register{$other_register} >= $comparison_value)
        || ( $operation eq '<=' && $register{$other_register} <= $comparison_value)
}

__DATA__
b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10
