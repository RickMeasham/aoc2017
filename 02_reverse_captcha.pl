#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

# Circular (append the first half to the end) 
my $input = $ARGV[0] . substr( $ARGV[0], 0, length( $ARGV[0] ) / 2);

# The number of digits we want to skip
my $halflen = length( $ARGV[0] ) / 2 - 1;

# Positive lookahead using the current match and skipping one less than half the length of the regex
my @matches = $input =~ m/(\d)(?=.{$halflen}\1)/g;

# Add 'em up
my $sum = 0;
$sum += $_ foreach @matches;

# Print 'em out
say join(' + ', @matches) . ' = ' . $sum;

