#!/usr/bin/env perl

use 5.10.0;
use strict;
use warnings;

use Data::Dumper;

my $tower;

my @input = grep { /./ } <>;

foreach my $line ( @input ){
    my( $program, $weight) = $line =~ /^(\w+) \((\d+)\)/;

    $tower->{ $program } = {
        program => $program,
        weight => $weight
    };
}
foreach my $line ( @input ){
    my( $program, $child_processes ) = $line =~ /^(\w+) \(\d+\)(?: -> ((?:\w+, )*(?:\w+)))?/;
    my @child_processes = grep /./, split(/, /, ($child_processes||''));

    foreach my $child_process ( @child_processes ){
        $tower->{ $child_process }{ parent } = $tower->{ $program };
        push( @{ $tower->{ $program }{ children } }, $tower->{ $child_process } );
    }
}

# To find the top-level program, we pick any at random and recurse up the tree until there's no parent program
my ($random_program) = keys %{$tower};
my $element = $tower->{ $random_program };
while( exists $element->{ parent } ){
    $element = $element->{ parent };
}
say "Top level program is " . $element->{program};


# For every element, record the weight of the stack (children + self)
sub weigh_elements {
    my $element = shift;

    my $child_weight = 0;
    if( ref $element->{ children } eq 'ARRAY' ) {
        foreach my $child ( @{$element->{ children }} ){
            $child_weight += weigh_elements( $child )
        }
    }
    $element->{stack_weight} = $child_weight + $element->{weight};
    return $element->{stack_weight};
}
weigh_elements( $element );

# Given a group of children, work out which one is the odd-one-out
sub find_odd_element {
    my @elements = @{+shift};

    my %children_by_weight;
    foreach my $child (@elements) {
        push( @{$children_by_weight{ $child->{stack_weight} }}, $child );
    }

    foreach my $weight (keys %children_by_weight) {
        # The weight with only one child is the odd one
        if (scalar( @{ $children_by_weight{$weight} } ) == 1) {
            return $children_by_weight{$weight}[0];
        }
    }
    return undef;
}

# Find the deepest point at which there is an odd element. That's the broken one.
sub find_deepest_break {
    my $element = shift;
    my $odd_element = find_odd_element( $element->{children} );

    # If there's an odd element, RECURSE! We need the deepest one
    if( defined $odd_element ){
        return find_deepest_break( $odd_element );
    }

    # If there's NO odd element, then WE are the odd one!
    return $element;
}
my $deepest_break = find_deepest_break( $element );

# Get all the siblings so we can work out what the weight _should_ be
my $siblings = $deepest_break->{parent}{children};
foreach my $sibling( @{$siblings} ){
    # If the sibling isn't the deepest break, we can use it to work out the correct weight of the deepest break
    if( $sibling->{program} ne $deepest_break->{program} ){
        say "Weight of " . $deepest_break->{program}. " should be " .  ( $deepest_break->{weight} + $sibling->{stack_weight} - $deepest_break->{stack_weight} );
        exit;
    }
}

