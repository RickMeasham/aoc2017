#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

use Data::Dumper;

my $stream = join('', <>);

# GARBAGE COLLECTION

# Part A - post operator is so pretty for this
# s/!.//g, s/<.*?>//g, s/,//g for $stream;

# Part B - couldn't be so sucinct. Damn you AoC!
$stream =~ s/!.//g;
my @garbage = $stream =~ m/<(.*?)>/g;
s/<.*?>//g, s/,//g for $stream;


# Now we just have braces left we can increment and decrement out depth on every character and add the depth to the score.
my $depth = 0;
my $score = 0;

while( $stream =~ s/^(.)// ){
	my $char = $1;

	if( $char eq '{' ){
		$depth++;
		$score += $depth;
	}
	else {
		$depth--;
	}
}


say "PART A Score: $score";

my $garbage_length = 0;
$garbage_length += length( $_ ) for @garbage;
say "PART B Garbage length: $garbage_length";
