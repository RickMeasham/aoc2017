#!/usr/bin/env perl

use 5.20.0;
use strict;
use warnings;

my @input = <STDIN>;

my $valid = 0;
foreach my $line ( @input ){
	my @words = split /\s+/, $line;

	my %words;
	@words{ @words } = ();

	if( scalar( @words ) == scalar( keys %words ) ){
		$valid++;
	}
}

say "There are $valid passphrases.";
